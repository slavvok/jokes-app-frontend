import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../user.service'

@Component({
  selector: 'app-generatejoke',
  templateUrl: './generatejoke.component.html',
  styleUrls: ['./generatejoke.component.css']
})
export class GeneratejokeComponent implements OnInit {
  new_joke: any;
  @Output() onChanged = new EventEmitter();


  constructor(private userService: UserService) { }

  ngOnInit(): void {

  }

  generateJoke() {
    this.userService.generateJoke().subscribe(
      response => {console.log(response);
        this.new_joke = response;
        this.onChanged.emit(response)}
    )
  }
}
