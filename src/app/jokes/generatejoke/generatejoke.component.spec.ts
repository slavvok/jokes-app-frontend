import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneratejokeComponent } from './generatejoke.component';

describe('GeneratejokeComponent', () => {
  let component: GeneratejokeComponent;
  let fixture: ComponentFixture<GeneratejokeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneratejokeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneratejokeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
