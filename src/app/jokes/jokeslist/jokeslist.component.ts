import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

import { UserService } from '../../user.service';

@Component({
  selector: 'app-jokeslist',
  templateUrl: './jokeslist.component.html',
  styleUrls: ['./jokeslist.component.css']
})
export class JokeslistComponent implements OnInit {
  jokes: any;
  isRemoved: boolean = false;
  removedId: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getJokesList().pipe(first()).subscribe(jokes_list => {
      this.jokes = jokes_list;
    })
  }

  removeJoke(id) {
    this.userService.removeJoke(id).subscribe(
      response => {
        if (response) {
          this.isRemoved = true;
          var removeId = this.jokes.map(function(item) {return item.id}).indexOf(id);
          this.jokes.splice(removeId, 1);
          this.removedId.next(id);
          this.removeNotification();
        }
      }
    )
  }

  removeNotification() {
    this.isRemoved = true;
    setTimeout( () => {this.isRemoved = false}, 5000);
  }

  getRemovedId() {
    return this.removedId.value;
  }

  onChanged(new_joke) {
    if(!new_joke.dublicate) {
      this.jokes.push(new_joke);
    }
  }
}
