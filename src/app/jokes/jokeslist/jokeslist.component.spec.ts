import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JokeslistComponent } from './jokeslist.component';

describe('JokeslistComponent', () => {
  let component: JokeslistComponent;
  let fixture: ComponentFixture<JokeslistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JokeslistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JokeslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
