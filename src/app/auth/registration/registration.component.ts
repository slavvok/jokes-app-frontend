import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { User } from '../../user';
import { UserService } from '../../user.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  @Input() user: User = {username: '', password: ''}
  returnUrl: string;

  constructor(private userService: UserService,
              private location: Location,
              private router: Router,
              private route: ActivatedRoute) {

    }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/jokeslist';
  }

  save(): void {
    this.userService.loginUser(this.user)
    .pipe(first())
    .subscribe(
      resp => {
        this.router.navigate([this.returnUrl]);
      }
    )
  }

  // goBack(): void {
	// 	this.location.back();
	// }

}
