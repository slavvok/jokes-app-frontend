import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { JokeslistComponent } from './jokes/jokeslist/jokeslist.component';
import { JwtInterceptor } from './auth/jwt.interceptor'
import { AppRoutingModule } from './app-routing.module';
import { GeneratejokeComponent } from './jokes/generatejoke/generatejoke.component';
import { RoutesComponent } from './routes/routes.component'

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    JokeslistComponent,
    GeneratejokeComponent,
    RoutesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
