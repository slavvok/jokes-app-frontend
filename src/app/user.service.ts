import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { User } from './user';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class UserService {
  private userUrl = 'http://165.22.75.130:8080'
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient,
              private router: Router) {
              this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')))
              this.currentUser = this.currentUserSubject.asObservable();
            }

  public get currentUserValue(): User {
      return this.currentUserSubject.value;
  }

  loginUser(user:User) {
    return this.http.post<any>(this.userUrl + '/auth/jwt', user, httpOptions)
      .pipe(map(user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
      }))
  }

  logout() {
          // remove user from local storage to log user out
          localStorage.removeItem('currentUser');
          this.currentUserSubject.next(null);
      }

  // refreshToken() {
  //   return this.http.post(this.userUrl + '/auth/refresh', )
  //     .pipe(map((user: User) => {
  //       localStorage.setItem('currentUser', JSON.stringify(user));
  //       this.currentUserSubject.next(user);
  //       return user;
  //     }))
  // }

  getJokesList() {
    return this.http.get(this.userUrl + '/get-jokes-list', httpOptions)
  }

  generateJoke() {
    return this.http.post(this.userUrl + '/generate-joke', httpOptions)
  }

  removeJoke(id) {
    return this.http.post(this.userUrl + `/remove-joke/${id}`, httpOptions)
  }
}
