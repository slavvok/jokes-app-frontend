import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { JokeslistComponent } from './jokes/jokeslist/jokeslist.component';
import { AuthGuard } from './auth/auth.guard';

const appRoutes: Routes = [
  {path: 'registration', component: RegistrationComponent},
  {path: 'jokeslist', component: JokeslistComponent, canActivate: [AuthGuard]},
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false
      }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
